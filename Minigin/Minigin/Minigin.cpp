#include "MiniginPCH.h"
// ReSharper disable once CppUnusedIncludeDirective
#include <vld.h>
#pragma comment(lib,"xinput.lib")
#include "SDL.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"

#include "Scene.h"
#include "GameObject.h"
#include "TextObject.h"
#include "RenderComponent.h"
#include "TextComponent.h"
#include "InputComponent.h"
#include "ColliderComponent.h"
#include "Time.h"
#include "Pacman.h"
#include "Ghost.h"


/**
 * \brief 
 * 0: = pill; 1 = walls; 2 = pacman; 3/4/5/6 = ghost; 7 = ghost p2; 8 = pacman p2; 9 = PowerPill
 */
const int g_Grid[12][16]
{
	{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },
	{ 1,4,0,0,0,0,0,0,0,0,0,0,0,0,5,1 },
	{ 1,0,9,1,1,0,1,1,0,1,0,1,1,9,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,0,1,1,1,0,1 },
	{ 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 },
	{ 1,0,1,1,1,0,1,0,1,1,0,1,1,1,0,1 },
	{ 1,0,1,1,1,0,1,0,1,1,0,1,1,1,0,1 },
	{ 1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,1 },
	{ 1,0,1,1,1,0,0,1,1,0,0,1,1,1,0,1 },
	{ 1,0,9,1,1,0,1,1,1,1,0,1,1,9,0,1 },
	{ 1,3,0,0,0,0,0,0,0,0,0,0,0,0,6,1 },
	{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },
};

void Initialize();
void LoadGame();
void Cleanup();

void PrepareGrid(std::shared_ptr<Scene> scene);
std::shared_ptr<Pacman> LoadPacman(float x, float y, std::shared_ptr<Scene> scene, bool isPlayer2);
std::shared_ptr<Ghost> LoadGhost(float x, float y, Ghost::Color color, std::shared_ptr<Scene> scene, bool isAi);
void LoadPill(float x, float y, std::shared_ptr<Scene> scene);
void LoadPowerPill(float x, float y, std::shared_ptr<Scene> scene);
void LoadWall(float x, float y, std::shared_ptr<Scene> scene);

#pragma warning( push )  
#pragma warning( disable : 4100 )  
int main(int argc, char* argv[]) {
#pragma warning( pop )

	Initialize();
	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");
	LoadGame();
	
	{
		auto& input = InputManager::GetInstance();
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();

		auto previous = std::chrono::high_resolution_clock::now();
		float lag = 0.0f;

		bool doContinue = true;
		while (doContinue)
		{
			auto current = std::chrono::high_resolution_clock::now();
			auto elapsed = std::chrono::duration_cast<std::chrono::duration<float, std::milli>>(current - previous).count();
			previous = current;
			lag += elapsed;

			Time::m_DeltaTime = elapsed / 1000;

			doContinue = input.ProcessInput();
			sceneManager.ProcessInput();

			while (lag > Time::m_MsPerFrame)
			{
				sceneManager.Update();
				sceneManager.LateUpdate();
				lag -= Time::m_MsPerFrame;
			}

			renderer.Render();

		}
	}

	Cleanup();
    return 0;
}

SDL_Window* window;

void PrepareGrid(std::shared_ptr<Scene> scene)
{
	std::shared_ptr<Pacman> pacman;
	std::shared_ptr<Ghost> red, blue, pink, orange;

	for (int i {}; i<12;i++)
	{
		for(int j{}; j<16;j++)
		{
			switch (g_Grid[i][j])
			{
			case 0:
				LoadPill(float(j*40),float(i * 40), scene);
				break;
			case 1:
				LoadWall(float(j * 40), float(i * 40), scene);
				break;
			case 2:
				pacman = LoadPacman(float(j * 40), float(i * 40), scene, false);
				break;
			case 3:
				red = LoadGhost(float(j * 40), float(i * 40), Ghost::Color::Red, scene, true);
				break;
			case 4:
				orange = LoadGhost(float(j * 40), float(i * 40), Ghost::Color::Orange, scene ,true);
				break;
			case 5:
				pink = LoadGhost(float(j * 40), float(i * 40), Ghost::Color::Pink, scene, true);
				break;
			case 6:
				blue = LoadGhost(float(j * 40), float(i * 40), Ghost::Color::Blue, scene, true);
				break;
			case 7:
				blue = LoadGhost(float(j * 40), float(i * 40), Ghost::Color::Blue, scene, false);
				break;
			case 8:
				pacman = LoadPacman(float(j * 40), float(i * 40), scene, true);
				break;
			case 9:
				LoadPowerPill(float(j * 40), float(i * 40), scene);
				break;
				
				default:
				break;
			}
		}
	}
	pacman->GetReferences();
	red->GetTarget("Pacman");
	blue->GetTarget("Pacman");
	orange->GetTarget("Pacman");
	pink->GetTarget("Pacman");
}

void Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::stringstream ss; ss << "SDL_Init Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,                    
		480,                    
		SDL_WINDOW_OPENGL       
	);
	if (window == nullptr) {
		std::stringstream ss; ss << "SDL_CreateWindow Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	Renderer::GetInstance().Init(window);
}

void LoadGame()
{
	
	auto scene = SceneManager::GetInstance().CreateScene("PacMan");
	SceneManager::GetInstance().SetActive(scene);
	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf",30);
	
	auto go = std::make_shared<GameObject>(scene,"Background");
	go->AddComponent(std::make_shared<RenderComponent>(go, true, SDL_Color{}));
	go->Init();
	go->GetComponent<RenderComponent>()->SetTexture("background.jpg");
	scene->Add(go);
	

	auto UIScore = std::make_shared<GameObject>(scene, "UIScore");
	UIScore->AddComponent(std::make_shared<TextComponent>("0", font, UIScore));
	scene->Add(UIScore);


	auto UILives = std::make_shared<GameObject>(scene, "UILives");
	UILives->AddComponent(std::make_shared<TextComponent>("0", font, UILives));
	UILives->GetComponent<TransformComponent>()->SetPosition(400, 0, 0);
	scene->Add(UILives);
	

	PrepareGrid(scene);
	//set UI last in vector so they get rendered on top 
	std::swap(scene->GetSceneObjects()[1], scene->GetSceneObjects().back());
	std::swap(scene->GetSceneObjects()[2], scene->GetSceneObjects()[scene->GetSceneObjects().size() - 2]);

}

void Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	SDL_Quit();
}

std::shared_ptr<Pacman> LoadPacman(float x, float y, std::shared_ptr<Scene> scene, bool isPlayer2)
{
	auto spawnPoint = std::make_shared<GameObject>(scene, "SpawnPoint");
	spawnPoint->Init();
	spawnPoint->GetComponent<TransformComponent>()->SetPosition(x + 2, y + 2, 0);
	scene->Add(spawnPoint);

	auto pacman = std::make_shared<Pacman>(scene);
	pacman->Init(pacman, glm::vec2{ 35,35 }, glm::vec2{ x + 2,y + 2}, isPlayer2);
	scene->Add(pacman);
	return pacman;
}

std::shared_ptr<Ghost> LoadGhost(float x, float y, Ghost::Color color, std::shared_ptr<Scene> scene, bool isAi)
{

	auto ghost = std::make_shared<Ghost>(scene);
	ghost->Init(ghost, glm::vec2{ 35,35 }, glm::vec2{ x + 2 ,y + 2}, isAi, color);
	scene->Add(ghost);
	return ghost;
}

void LoadPill(float x, float y, std::shared_ptr<Scene> scene)
{
	auto pill = std::make_shared<GameObject>(scene, "Pill");
	pill->AddComponent(std::make_shared<ColliderComponent>(pill, glm::vec2{ 20,20 }, true, true));
	pill->AddComponent(std::make_shared<RenderComponent>(pill, false, SDL_Color{225,225,0,255}));
	pill->Init();
	pill->GetComponent<TransformComponent>()->SetPosition(x + 10, y + 10 , 0);
	scene->Add(pill);
}

void LoadPowerPill(float x, float y, std::shared_ptr<Scene> scene)
{
	auto pill = std::make_shared<GameObject>(scene, "PowerPill");
	pill->AddComponent(std::make_shared<ColliderComponent>(pill, glm::vec2{ 30,30 }, true, true));
	pill->AddComponent(std::make_shared<RenderComponent>(pill, false, SDL_Color{ 225,0,0,255 }));
	pill->Init();
	pill->GetComponent<TransformComponent>()->SetPosition(x + 5, y + 5, 0);
	scene->Add(pill);
}

void LoadWall(float x, float y, std::shared_ptr<Scene> scene)
{
	auto wall = std::make_shared<GameObject>(scene, "Wall");
	wall->AddComponent(std::make_shared<ColliderComponent>(wall, glm::vec2{ 40,40 }, true, false));
	wall->AddComponent(std::make_shared<RenderComponent>(wall,false, SDL_Color{ 0,0,255,255 }));
	wall->Init();
	wall->GetComponent<TransformComponent>()->SetPosition(x, y, 0);
	scene->Add(wall);
}
