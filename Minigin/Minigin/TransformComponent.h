#pragma once
#include "BaseComponent.h"
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#pragma warning(pop)

class TransformComponent :public BaseComponent
{
public:
	explicit TransformComponent(std::shared_ptr<GameObject> parent);
	TransformComponent();
	void Init() override;
	void Update() override;
	void LateUpdate() override;

	const glm::vec3& GetPosition() const { return m_Position; }
	void SetPosition(float x, float y, float z);
	void UpdatePosition(float x, float y);
	void ReturnToLastPos();

private:
	glm::vec3 m_Position;
	glm::vec3 m_LastPos;
};



