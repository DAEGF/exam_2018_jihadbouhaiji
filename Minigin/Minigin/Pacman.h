#pragma once
#include "GameObject.h"
#include "TextComponent.h"
#include "ColliderComponent.h"

class Pacman :
	public GameObject
{
public:
	Pacman(std::shared_ptr<Scene> scene);
	~Pacman();

	void Init(std::shared_ptr<GameObject> self,const glm::vec2& size,const glm::vec2& pos, bool isPlayer2);
	void Update() override;
	void LateUpdate() override;
	void GetReferences();

	void Reset();
private:
	int m_Lives;
	int m_Score;
	bool m_ScorehasChanged;
	bool m_LiveshasChanged;

	std::weak_ptr<TextComponent> m_pUILives;
	std::weak_ptr<TextComponent> m_pUIScore;
	std::weak_ptr<TransformComponent> m_pSpawnPoint;

	void ProcessTriggerHit(std::weak_ptr<ColliderComponent> collider, std::weak_ptr<ColliderComponent> trigger);
};

