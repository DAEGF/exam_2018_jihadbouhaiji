#include "MiniginPCH.h"
#include "Pacman.h"
#include "RenderComponent.h"
#include "InputComponent.h"
#include "Ghost.h"


Pacman::Pacman(std::shared_ptr<Scene> scene) :
	GameObject(scene, "Pacman"),
	m_Lives(3),
	m_Score(0), 
m_ScorehasChanged(false), 
m_LiveshasChanged(false)
{
}


Pacman::~Pacman() = default;

void Pacman::Init(std::shared_ptr<GameObject> self, const glm::vec2& size, const glm::vec2& pos, bool isPlayer2)
{
	AddComponent(std::make_shared<RenderComponent>(self,true, SDL_Color{}));
	AddComponent(std::make_shared<InputComponent>(self));
	AddComponent(std::make_shared<PhysicsComponent>(self, size, 20.f, true));
	GameObject::Init();

	GetComponent<RenderComponent>()->SetTexture("Original_PacMan.png");
	GetComponent<TransformComponent>()->SetPosition(pos.x, pos.y, 0.0f);
	GetComponent<ColliderComponent>()->MustRender(true);

	if (!isPlayer2)
	{
		GetComponent<InputComponent>()->AddInput(Input::KeyUp, std::make_unique<UpCommand>());
		GetComponent<InputComponent>()->AddInput(Input::KeyLeft, std::make_unique<LeftCommand>());
		GetComponent<InputComponent>()->AddInput(Input::KeyRight, std::make_unique<RightCommand>());
		GetComponent<InputComponent>()->AddInput(Input::KeyDown, std::make_unique<DownCommand>());

	}
	else
	{
		GetComponent<InputComponent>()->AddInput(Input::ButtonY, std::make_unique<UpCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonX, std::make_unique<LeftCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonB, std::make_unique<RightCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonA, std::make_unique<DownCommand>());
	}

}

void Pacman::Update()
{
	GameObject::Update();
	auto collider = GetComponent<ColliderComponent>();
	if (!collider->GetTriggerHit().expired()) ProcessTriggerHit(collider, collider->GetTriggerHit());
}

void Pacman::LateUpdate()
{
	if (m_LiveshasChanged)
	{
		if(!m_pUILives.expired()) m_pUILives.lock()->SetText(std::to_string(m_Lives));
		m_LiveshasChanged = false;
	}

	if(m_ScorehasChanged)
	{
		if(!m_pUIScore.expired()) m_pUIScore.lock()->SetText(std::to_string(m_Score));
		m_ScorehasChanged = false;
	}
	GameObject::LateUpdate();

}

void Pacman::GetReferences()
{
	m_pUILives = GetScene().lock()->GetGameObjByName("UILives")->GetComponent<TextComponent>();
	m_pUIScore = GetScene().lock()->GetGameObjByName("UIScore")->GetComponent<TextComponent>();
	m_pSpawnPoint = GetScene().lock()->GetGameObjByName("SpawnPoint")->GetComponent<TransformComponent>();
}

void Pacman::Reset()
{
	GetComponent<TransformComponent>()->SetPosition(m_pSpawnPoint.lock()->GetPosition().x,
	                                                m_pSpawnPoint.lock()->GetPosition().y,
	                                                m_pSpawnPoint.lock()->GetPosition().z);
}

void Pacman::ProcessTriggerHit(std::weak_ptr<ColliderComponent> collider, std::weak_ptr<ColliderComponent> trigger)
{
	if (trigger.lock()->GetParent().lock()->GetName() == "Pill")
	{
		m_Score++;
		m_ScorehasChanged = true;
		trigger.lock()->GetParent().lock()->Destroy();
		collider.lock()->TriggerHitProcessed();
		return;
	}

	if (trigger.lock()->GetParent().lock()->GetName() == "PowerPill")
	{
		m_Score += 5;
		m_ScorehasChanged = true;
		
		std::vector<std::shared_ptr<GameObject>> allGhosts = m_Scene.lock()->GetAllGameObjByName("Ghost");
		for(auto ghost : allGhosts)
		{
			std::static_pointer_cast<Ghost>(ghost)->SetScared(true);
		}
		
		trigger.lock()->GetParent().lock()->Destroy();
		collider.lock()->TriggerHitProcessed();
		return;
	}
		
	if (trigger.lock()->GetParent().lock()->GetName() == "Ghost")
	{
		if (std::static_pointer_cast<Ghost>(trigger.lock()->GetParent().lock())->IsScared())
		{
			m_Score += 10;
			m_ScorehasChanged = true;
			trigger.lock()->GetParent().lock()->Destroy();
			collider.lock()->TriggerHitProcessed();
			return;
		}
		
		m_Lives--;
		m_LiveshasChanged = true;
		Reset();
		collider.lock()->TriggerHitProcessed();
		

	}

	if(m_Lives < 0)
	{
		Destroy();
	}
}




