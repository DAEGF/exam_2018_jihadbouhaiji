#pragma once
#include "SceneManager.h"
#include "GameObject.h"


class SceneObject;
class Scene
{
	friend std::shared_ptr<Scene> SceneManager::CreateScene(const std::string& name);
public:
	void Add(const std::shared_ptr<GameObject>& object);

	void Update();
	void LateUpdate();
	void Render() const;
	void ProcessInput();
	std::vector<std::shared_ptr<GameObject>> &GetSceneObjects();
	std::shared_ptr<GameObject> GetGameObjByName(std::string name);
	std::vector<std::shared_ptr<GameObject>> GetAllGameObjByName(std::string name);
	~Scene();
	Scene(const Scene& other) = delete;
	Scene(Scene&& other) = delete;
	Scene& operator=(const Scene& other) = delete;
	Scene& operator=(Scene&& other) = delete;

private:
	explicit Scene(const std::string& name);

	std::string m_Name{};
	std::vector < std::shared_ptr<GameObject>> m_Objects{};

	static unsigned int idCounter;
};



