#include "MiniginPCH.h"
#include "ColliderComponent.h"
#include "GameObject.h"
#include "Renderer.h"


ColliderComponent::ColliderComponent(std::shared_ptr<GameObject> parent, glm::vec2 size, bool isStatic, bool isTrigger):
BaseComponent(parent),
m_Size(size),
m_IsStatic(isStatic),
m_ToRender(false),
m_isTrigger(isTrigger)
{
	m_Position = m_pParent.lock()->GetComponent<TransformComponent>();
}


ColliderComponent::~ColliderComponent() = default;

void ColliderComponent::Init()
{
	
}

void ColliderComponent::Update()
{
	if(!m_IsStatic)
	{
		std::vector<std::shared_ptr<GameObject>> AllObjInScene = m_pParent.lock()->GetScene().lock()->GetSceneObjects();
		for (unsigned int i{}; i < AllObjInScene.size(); i++)
		{
			std::shared_ptr<ColliderComponent> collider{ AllObjInScene[i]->GetComponent<ColliderComponent>() };

			if (collider != nullptr && collider.get() != this)
			{
				if (IsIntersecting(collider))
				{
					if (!collider->m_isTrigger)
					{
						m_Position.lock()->ReturnToLastPos();
						return;
					}
					m_TriggerHit = collider;
				
				}
			}
		}
	}
}

void ColliderComponent::Render() const
{
	if(m_ToRender) Renderer::GetInstance().RenderRectangle(m_Position.lock()->GetPosition().x, m_Position.lock()->GetPosition().y, m_Size.x,m_Size.y);
}

bool ColliderComponent::IsIntersecting(std::weak_ptr<ColliderComponent> other) const
{
	// 0 = x, 1 = y + height, 2 = x + width, 3 = y
	float VerticesA[4]{ m_Position.lock()->GetPosition().x, m_Position.lock()->GetPosition().y + m_Size.y, 
						m_Position.lock()->GetPosition().x + m_Size.x,  m_Position.lock()->GetPosition().y};

	float VerticesB[4]{ other.lock()->m_Position.lock()->GetPosition().x, other.lock()->m_Position.lock()->GetPosition().y + other.lock()->m_Size.y,
						other.lock()->m_Position.lock()->GetPosition().x + other.lock()->m_Size.x,  other.lock()->m_Position.lock()->GetPosition().y };

	if ((VerticesA[2] < VerticesB[0]) || (VerticesA[0] > VerticesB[2]))
	{
		return false;
	}
	
	if ((VerticesA[3] > VerticesB[1]) || (VerticesA[1] < VerticesB[3]))
	{
		
		return false;

	}

	return true;

}

bool ColliderComponent::WillIntersect(std::weak_ptr<ColliderComponent> other, const glm::vec3& futurePosition) const
{
	// 0 = x, 1 = y + height, 2 = x + width, 3 = y
	float VerticesA[4]{ futurePosition.x, futurePosition.y + m_Size.y,
						futurePosition.x + m_Size.x, futurePosition.y };
	float VerticesB[4]{ other.lock()->m_Position.lock()->GetPosition().x, other.lock()->m_Position.lock()->GetPosition().y + other.lock()->m_Size.y,
						other.lock()->m_Position.lock()->GetPosition().x + other.lock()->m_Size.x,  other.lock()->m_Position.lock()->GetPosition().y };

	if ((VerticesA[2] < VerticesB[0]) || (VerticesA[0] > VerticesB[2]))
	{
		return false;
	}

	if ((VerticesA[3] > VerticesB[1]) || (VerticesA[1] < VerticesB[3]))
	{

		return false;

	}

	return true;
}

void ColliderComponent::MustRender(bool set)
{
	m_ToRender = set;
}

void ColliderComponent::SetSize(float x, float y)
{
	m_Size.x = x;
	m_Size.y = y;
}

bool ColliderComponent::IsTrigger() const
{
	return m_isTrigger;
}

glm::vec2 ColliderComponent::GetSize() const
{
	return m_Size;
}

std::weak_ptr<ColliderComponent> ColliderComponent::GetTriggerHit() const
{
	return m_TriggerHit;
}

void ColliderComponent::TriggerHitProcessed()
{
	m_TriggerHit.reset();
}


