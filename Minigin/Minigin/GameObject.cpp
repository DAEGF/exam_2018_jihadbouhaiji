#include "MiniginPCH.h"
#include "GameObject.h"
#include "BaseComponent.h"
#include "RenderComponent.h"
#include "InputComponent.h"
#include "TextComponent.h"
#include "AIInputComponent.h"

GameObject::~GameObject() = default;

GameObject::GameObject():
	m_Transform{ std::make_shared<TransformComponent>() },
	m_Scene{}
{
}

GameObject::GameObject(std::shared_ptr<Scene> scene,  std::string name) :
	m_Transform{ std::make_shared<TransformComponent>() },
	m_Scene{scene},
	m_Name{name}
{
}

void GameObject::Init()
{
	int Physicsindex{};
	int ColliderIndex{};
	m_Transform->Init();
	for (unsigned int i{}; i<m_Components.size(); i++)
	{
		m_Components[i]->Init();
		if (typeid(ColliderComponent) == typeid(*m_Components[i])) ColliderIndex = i;
		else if (typeid(PhysicsComponent) == typeid(*m_Components[i])) Physicsindex = i;
	}

	if (ColliderIndex < Physicsindex) std::swap(m_Components[ColliderIndex], m_Components[Physicsindex]);
}

void GameObject::Update()
{
	m_Transform->Update();
	for(unsigned int i{};i<m_Components.size();i++)
	{
		m_Components[i]->Update();
	}
}

void GameObject::LateUpdate()
{
	m_Transform->LateUpdate();
	for (unsigned int i{}; i<m_Components.size(); i++)
	{
		m_Components[i]->LateUpdate();
	}
}

void GameObject::Render()
{
	for (unsigned int i{}; i<m_Components.size(); i++)
	{
		m_Components[i]->Render();
	}
}

void GameObject::ProcessInput()
{
	if (GetComponent<InputComponent>() != nullptr) GetComponent<InputComponent>()->HandleInput();
	else if (GetComponent<AIInputComponent>() != nullptr) GetComponent<AIInputComponent>()->HandleInput();
}

void GameObject::Destroy()
{
	auto& AllObjInScene = m_Scene.lock()->GetSceneObjects();
	for (unsigned int i{}; i < AllObjInScene.size() ; i++)
	{
		if (AllObjInScene[i].get() == this)
		{
			AllObjInScene.erase(AllObjInScene.begin() + i);
			return;
		}
	}
}


std::shared_ptr<BaseComponent> GameObject::AddComponent(std::shared_ptr<BaseComponent> compToAdd)
{
	if (typeid(TransformComponent) == typeid(compToAdd))
	{
		std::cout << "cant add another Transform" << std::endl;
		return nullptr;
	}

	for(auto component : m_Components)
	{
		
		if(typeid(*component) == typeid(compToAdd))
		{
			std::cout << "Cant add same component twice" << std::endl;
			return nullptr;
		}
	}
	m_Components.push_back(compToAdd);
	return compToAdd;
}

void GameObject::RemoveComponent(std::shared_ptr<BaseComponent> compToRemove)
{
	auto result = std::find(m_Components.begin(), m_Components.end(), compToRemove);

	if (result == m_Components.end())
	{
		std::cout << "Component not found" << std::endl;
		return;
	}

	m_Components.erase(result);
}

std::weak_ptr<Scene> GameObject::GetScene() const
{
	return m_Scene;
}

std::string GameObject::GetName() const
{
	return m_Name;
}




