#pragma once
#include "BaseComponent.h"
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include "ColliderComponent.h"
#pragma warning(pop)

class PhysicsComponent :
	public BaseComponent
{
public:
	PhysicsComponent(std::shared_ptr<GameObject> parent, glm::vec2 size, float speed, bool isTrigger);
	~PhysicsComponent();

	void Init() override;
	void Update() override;
	void SetDesiredVelocity(float x, float y);
	void SetVelocity(float x, float y);
	void ClearVelocity();
private:
	glm::vec3 m_Velocity;
	glm::vec3 m_DesiredVelocity;
	std::weak_ptr<ColliderComponent> m_Collider;
	std::weak_ptr<TransformComponent> m_ParentTransform;
	float m_Speed;
};

