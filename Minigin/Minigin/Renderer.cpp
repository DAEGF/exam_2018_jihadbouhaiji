#include "MiniginPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2D.h"

void Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_Renderer == nullptr) {
		std::stringstream ss; ss << "SDL_CreateRenderer Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
}

void Renderer::Render()
{
	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void Renderer::RenderTexture(const Texture2D& texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void Renderer::RenderTexture(const Texture2D& texture, const float x, const float y, const float width, const float height) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	dst.w = static_cast<int>(width);
	dst.h = static_cast<int>(height);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void Renderer::RenderRectangle(float x, float y, float width, float height) const
{
	SDL_Rect toDraw;
	toDraw.x = static_cast<int>(x);
	toDraw.y = static_cast<int>(y);
	toDraw.w = static_cast<int>(width);
	toDraw.h = static_cast<int>(height);
	RenderRectangle(toDraw);
}

void Renderer::RenderRectangle(const SDL_Rect& rectToDraw) const
{
	SDL_SetRenderDrawColor(m_Renderer, 255, 0, 0, 1);
	SDL_RenderDrawRect(m_Renderer, &rectToDraw);
}

void Renderer::RenderFilledRectangle(float x, float y, float width, float height,const SDL_Color& color) const
{
	SDL_SetRenderDrawColor(m_Renderer, color.r, color.g, color.b, color.a);
	SDL_Rect toDraw;
	toDraw.x = static_cast<int>(x);
	toDraw.y = static_cast<int>(y);
	toDraw.w = static_cast<int>(width);
	toDraw.h = static_cast<int>(height);
	SDL_RenderFillRect(m_Renderer, &toDraw);
}
