#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"
#include "InputComponent.h"

unsigned int Scene::idCounter = 0;

Scene::Scene(const std::string& name) : m_Name(name) {}

Scene::~Scene()
{
	//m_Objects.clear();
}

void Scene::Add(const std::shared_ptr<GameObject>& object)
{
	m_Objects.push_back(object);
}

void Scene::Update()
{
	for(auto gameObject : m_Objects)
	{
		if(gameObject != nullptr) gameObject->Update();
	}
}

void Scene::LateUpdate()
{
	for (auto gameObject : m_Objects)
	{
		if (gameObject != nullptr) gameObject->LateUpdate();
	}
}

void Scene::Render() const
{
	for (const auto gameObject : m_Objects)
	{
		if (gameObject != nullptr) gameObject->Render();
	}
}

void Scene::ProcessInput()
{
	for (const auto gameObject : m_Objects)
	{
		gameObject->ProcessInput();
	}
	
}

std::vector<std::shared_ptr<GameObject>>& Scene::GetSceneObjects()
{
	return m_Objects;
}

std::shared_ptr<GameObject> Scene::GetGameObjByName(std::string name)
{
	for (unsigned int i{}; i < m_Objects.size(); i++)
	{
		if(m_Objects[i]->GetName() == name)
		{
			return m_Objects[i];
		}
	}
	return nullptr;
}

std::vector<std::shared_ptr<GameObject>> Scene::GetAllGameObjByName(std::string name)
{
	std::vector<std::shared_ptr<GameObject>> objsFound{};
	for (unsigned int i{}; i < m_Objects.size(); i++)
	{
		if (m_Objects[i]->GetName() == name)
		{
			objsFound.push_back(m_Objects[i]);
		}
	}
	return objsFound;
}

