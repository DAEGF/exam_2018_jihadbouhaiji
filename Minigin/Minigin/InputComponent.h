#pragma once
#include <XInput.h>
#include "Command.h"
#include "BaseComponent.h"
#include <map>
#include "InputManager.h"

class InputComponent : public BaseComponent
{

public:
	explicit InputComponent(std::shared_ptr<GameObject> parent);
	void Init() override;
	void Update() override;
	void AddInput(Input inputToAdd, std::shared_ptr<Command> commandToAdd);
	virtual void HandleInput();
	InputComponent() = default;


private:
	std::vector<std::pair<Input,std::shared_ptr<Command>>> m_Inputs;
	XINPUT_STATE m_CurrentState;
	
};

