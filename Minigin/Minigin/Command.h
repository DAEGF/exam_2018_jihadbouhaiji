#pragma once
#include "GameObject.h"
#include "PhysicsComponent.h"

class Command
{
public:
	virtual ~Command() = default;
	virtual void Execute(std::shared_ptr<GameObject> object) = 0;
};

class UpCommand : public Command
{
	void Execute(std::shared_ptr<GameObject> object) override
	{
		auto physics{ object->GetComponent<PhysicsComponent>() };
		if (physics != nullptr) physics->SetDesiredVelocity(0.f, -1.f);
	}
};

class DownCommand : public Command
{
	void Execute(std::shared_ptr<GameObject> object) override
	{
		auto physics{ object->GetComponent<PhysicsComponent>() };
		if (physics != nullptr) physics->SetDesiredVelocity(0.f, 1.f);
	}
};

class LeftCommand : public Command
{
	void Execute(std::shared_ptr<GameObject> object) override
	{
		auto physics{ object->GetComponent<PhysicsComponent>() };
		if (physics != nullptr) physics->SetDesiredVelocity(-1.f, 0.f);
	}
};

class RightCommand : public Command
{
	void Execute(std::shared_ptr<GameObject> object) override
	{
		auto physics{ object->GetComponent<PhysicsComponent>() };
		if (physics != nullptr) physics->SetDesiredVelocity(1.f, 0.f);
	}
};

class ClearMovement : public Command
{
	void Execute(std::shared_ptr<GameObject> object) override
	{
		auto physics{ object->GetComponent<PhysicsComponent>() };
		if (physics != nullptr) physics->ClearVelocity();
	}
};