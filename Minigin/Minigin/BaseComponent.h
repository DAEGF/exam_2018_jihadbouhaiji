#pragma once

class GameObject;

class BaseComponent
{
public:
	BaseComponent(std::shared_ptr<GameObject> parent);
	virtual ~BaseComponent() = default;

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void LateUpdate() {};
	virtual void Render() const {};
	void SetParent(std::shared_ptr<GameObject> parent) { m_pParent = parent; }
	std::weak_ptr<GameObject> GetParent() { return m_pParent; }

protected:
	std::weak_ptr<GameObject> m_pParent;
	int m_Idx;
};

inline BaseComponent::BaseComponent(std::shared_ptr<GameObject> parent)
{
	m_pParent = parent;
}

