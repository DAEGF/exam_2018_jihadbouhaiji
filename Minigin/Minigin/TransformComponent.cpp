#include "MiniginPCH.h"
#include "TransformComponent.h"

TransformComponent::TransformComponent(std::shared_ptr<GameObject> parent) :
BaseComponent(parent),
m_Position{},
m_LastPos{}
{
}

TransformComponent::TransformComponent()
	: BaseComponent{ nullptr }
{
}

void TransformComponent::Init()
{
	m_Position = glm::vec3{ 0,0,0 };
}

void TransformComponent::Update()
{
	
}

void TransformComponent::LateUpdate()
{
	m_LastPos = m_Position;
}

void TransformComponent::SetPosition(float x, float y, float z)
{
	m_Position.x = x;
	m_Position.y = y;
	m_Position.z = z;

	m_LastPos = m_Position;
}

void TransformComponent::UpdatePosition(float x, float y)
{
	m_Position.x += x;
	m_Position.y += y;
}

void TransformComponent::ReturnToLastPos()
{
	m_Position = m_LastPos;
}
