#pragma once
#include "InputComponent.h"
class AIInputComponent :
	public InputComponent
{
public:
	
	enum class Commands
	{
		Up,
		Down,
		Left,
		Right
	};
	
	AIInputComponent(std::shared_ptr<GameObject> parent);
	~AIInputComponent();

	void Init() override;
	void HandleInput() override;
	void Update() override;
	void OverrideCommand(Commands commandToOverrideTo);
	void GetTargetByName(std::string name);
	Commands GetCurrentCommand() const;

private: 
	std::vector<std::unique_ptr<Command>> m_Commands;
	std::weak_ptr<TransformComponent> m_TargetLocation;
	std::weak_ptr<TransformComponent> m_MyLocation;
	Commands m_CurrentCommand;
};

