#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>


bool InputManager::ProcessInput()
{
	ZeroMemory(&m_CurrentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &m_CurrentState);

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
		
	}

	return true;
}

bool InputManager::IsPressed(Input toCheck) const
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);

	switch (toCheck)
	{
	case Input::ButtonA:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	case Input::ButtonB:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case Input::ButtonX:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case Input::ButtonY:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	
	case Input::KeyUp:
		return state[SDL_SCANCODE_UP];
	case Input::KeyDown:
		return state[SDL_SCANCODE_DOWN];
	case Input::KeyLeft:
		return state[SDL_SCANCODE_LEFT];
	case Input::KeyRight:
		return state[SDL_SCANCODE_RIGHT];
	
	default: return false;
	}
}

