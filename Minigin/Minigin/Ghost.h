#pragma once
#include "GameObject.h"
#include "ColliderComponent.h"

class Ghost :
	public GameObject
{
public:
	enum class Color
	{
		Red,
		Blue,
		Orange,
		Pink
	};

	Ghost(std::shared_ptr<Scene> scene);
	~Ghost();

	void Init(std::shared_ptr<GameObject> self, const glm::vec2& size, const glm::vec2& pos, bool isAi, Color color);
	void GetTarget(std::string name);
	void LateUpdate() override;

	void SetScared(bool toSet);
	bool IsScared() const;

private:
	bool m_isAi;
	bool m_IsScared;
	float m_ScareTimer;
	float m_ScareTimerMax;
};

