#pragma once
#include "BaseComponent.h"
#include "Texture2D.h"
#include "Font.h"


class TextComponent :
	public BaseComponent
{
public:
	explicit TextComponent(const std::string& text, std::shared_ptr<Font> font, std::shared_ptr<GameObject> parent);

	void Init() override;
	void Update() override;
	void Render() const override;

	void SetText(std::string text);
	void SetFont(std::shared_ptr<Font> font);
	void SetColor(UINT8 r, UINT8 g, UINT8 b);

	~TextComponent();

private:
	bool m_HasChanged;
	std::string m_Text;
	std::shared_ptr<Font> m_Font;
	std::shared_ptr<Texture2D> m_pTexture;
	SDL_Color m_Color;
};

