#include "MiniginPCH.h"
#include "PhysicsComponent.h"
#include "GameObject.h"
#include "Time.h"


PhysicsComponent::PhysicsComponent(std::shared_ptr<GameObject> parent, glm::vec2 size, float speed,bool isTrigger):
BaseComponent(parent),
m_Speed(speed)
{
	if (m_pParent.lock()->GetComponent<ColliderComponent>() == nullptr) m_Collider = std::static_pointer_cast<ColliderComponent>(m_pParent.lock()->AddComponent(std::make_shared<ColliderComponent>(parent,size,false,isTrigger)));
	else m_Collider = m_pParent.lock()->GetComponent<ColliderComponent>();

	m_ParentTransform = m_pParent.lock()->GetComponent<TransformComponent>();
}


PhysicsComponent::~PhysicsComponent() = default;

void PhysicsComponent::Init()
{
	m_Collider.lock()->MustRender(true);
}

void PhysicsComponent::Update()
{
	const glm::vec3 futurePos = m_ParentTransform.lock()->GetPosition() + m_DesiredVelocity;
	
	auto AllObjInScene = m_pParent.lock()->GetScene().lock()->GetSceneObjects();
	
	for (unsigned int i{}; i < AllObjInScene.size(); i++)
	{
		std::shared_ptr<ColliderComponent> collider{ 
			AllObjInScene[i]->GetComponent<ColliderComponent>() };

		if (!collider && collider.get() != m_Collider.lock().get())
		{
			if(!collider->IsTrigger())
			{
				if (m_Collider.lock()->WillIntersect(collider, futurePos))
				{
					m_ParentTransform.lock()->UpdatePosition(
						m_Velocity.x * m_Speed * Time::m_FixedDeltaTime, 
						m_Velocity.y * m_Speed * Time::m_FixedDeltaTime);
					return;
				}
			}
		}
	}
	m_Velocity = m_DesiredVelocity;
	m_ParentTransform.lock()->UpdatePosition(
		m_Velocity.x * m_Speed * Time::m_FixedDeltaTime, 
		m_Velocity.y * m_Speed * Time::m_FixedDeltaTime);
}

void PhysicsComponent::SetDesiredVelocity(float x, float y)
{
	m_DesiredVelocity.x = x;
	m_DesiredVelocity.y = y;
	m_DesiredVelocity.z = 0;
}

void PhysicsComponent::SetVelocity(float x, float y)
{
	m_Velocity.x = x;
	m_Velocity.y = y;
}

void PhysicsComponent::ClearVelocity()
{
	m_DesiredVelocity = glm::vec3{ 0,0,0 };
}
