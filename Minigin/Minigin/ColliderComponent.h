#pragma once
#include "BaseComponent.h"
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/glm.hpp>
#pragma warning(pop)

#include "TransformComponent.h"
#include <SDL.h>

class ColliderComponent :
	public BaseComponent
{
public:
	ColliderComponent(std::shared_ptr<GameObject> parent, glm::vec2 size, bool isStatic, bool isTrigger);
	~ColliderComponent();

	void Init() override;
	void Update() override;
	void Render() const override;

	bool IsIntersecting(std::weak_ptr<ColliderComponent> other) const;
	bool WillIntersect(std::weak_ptr<ColliderComponent> other, const glm::vec3& futurePosition) const;
	void MustRender(bool set);
	void SetSize(float x, float y);
	bool IsTrigger() const;

	glm::vec2 GetSize() const;
	std::weak_ptr<ColliderComponent> GetTriggerHit() const;
	void TriggerHitProcessed();

private:
	glm::vec2 m_Size;
	std::weak_ptr<TransformComponent> m_Position;
	std::weak_ptr<ColliderComponent> m_TriggerHit;
	
	bool m_IsStatic;
	bool m_isTrigger;
	bool m_ToRender;
};

