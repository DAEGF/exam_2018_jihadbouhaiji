#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"


void SceneManager::Update()
{
	
	m_ActiveScene.lock()->Update();
	
}

void SceneManager::LateUpdate()
{
	
	m_ActiveScene.lock()->LateUpdate();
	
}

void SceneManager::Render()
{
	
	m_ActiveScene.lock()->Render();
	
}

void SceneManager::ProcessInput()
{
	
	m_ActiveScene.lock()->ProcessInput();
	
}

void SceneManager::SetActive(std::shared_ptr<Scene> activeScene)
{
	m_ActiveScene = activeScene;
}

std::shared_ptr<Scene> SceneManager::CreateScene(const std::string& name)
{
	const auto scene = std::shared_ptr<Scene>(new Scene(name));
	m_Scenes.push_back(scene);
	return scene;
}
