#pragma once
#include <memory>
#include "SceneObject.h"
#include "BaseComponent.h"
#include "TransformComponent.h"
#include "Scene.h"

class GameObject : public SceneObject
	{
	public:
		GameObject();
		GameObject(std::shared_ptr<Scene> scene,std::string m_Name);

	virtual void Init();
		void Update() override;
		void LateUpdate() override;
		void Render() override;
		void ProcessInput() override;
		void Destroy();

		std::shared_ptr<BaseComponent> AddComponent(std::shared_ptr<BaseComponent> compToAdd);
		void RemoveComponent(std::shared_ptr<BaseComponent> compToRemove);

		std::weak_ptr<Scene> GetScene() const;
		std::string GetName() const;

		template<class T> std::shared_ptr<T> GetComponent()
		{
			
			for(auto component : m_Components)
			{
				if (typeid(*component) == typeid(T))
					return std::static_pointer_cast<T>(component);
			}

			return nullptr;
		}
		
		template<> std::shared_ptr<TransformComponent> GetComponent<TransformComponent>()
		{
			return m_Transform;
		}

		
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	protected:
		std::shared_ptr<TransformComponent> m_Transform = nullptr;
		std::vector<std::shared_ptr<BaseComponent>> m_Components;
		std::weak_ptr<Scene> m_Scene;
		std::string m_Name;
	};
