#include "MiniginPCH.h"
#include "AIInputComponent.h"


AIInputComponent::AIInputComponent(std::shared_ptr<GameObject> parent):
InputComponent(parent),
m_CurrentCommand{}
{
	
}

AIInputComponent::~AIInputComponent() = default;

void AIInputComponent::Init()
{
	m_Commands.push_back(std::make_unique<UpCommand>());
	m_Commands.push_back(std::make_unique<DownCommand>());
	m_Commands.push_back(std::make_unique<LeftCommand>());
	m_Commands.push_back(std::make_unique<RightCommand>());
	m_MyLocation = m_pParent.lock()->GetComponent<TransformComponent>();
}

void AIInputComponent::HandleInput()
{
	m_Commands[int(m_CurrentCommand)]->Execute(m_pParent.lock());
}

void AIInputComponent::Update()
{
	glm::vec3 vectorToPacman{};
	if(!m_TargetLocation.expired()) vectorToPacman = m_TargetLocation.lock()->GetPosition() - m_MyLocation.lock()->GetPosition();

	
	if (abs(vectorToPacman.x) > abs(vectorToPacman.y))	
	{
		if (vectorToPacman.x > 0) OverrideCommand(Commands::Right);
		else OverrideCommand(Commands::Left);
	}
	else
	{
		if (vectorToPacman.y < 0) OverrideCommand(Commands::Up);
		else OverrideCommand(Commands::Down);
	}
}

void AIInputComponent::OverrideCommand(Commands commandToOverrideTo)
{
	m_CurrentCommand = commandToOverrideTo;
}

void AIInputComponent::GetTargetByName(std::string name)
{
	m_TargetLocation = m_pParent.lock()->GetScene().lock()->GetGameObjByName(name)->GetComponent<TransformComponent>();
}

AIInputComponent::Commands AIInputComponent::GetCurrentCommand() const
{
	return m_CurrentCommand;
}


