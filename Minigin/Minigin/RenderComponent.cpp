#include "MiniginPCH.h"
#include "RenderComponent.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "GameObject.h"
#include "ColliderComponent.h"

RenderComponent::RenderComponent(std::shared_ptr<GameObject> parent, bool renderTexture, SDL_Color color) :
BaseComponent(parent),
m_pTexture{ nullptr },
m_RenderTexture{renderTexture},
m_RectColor{color}
{}

RenderComponent::~RenderComponent() = default;

void RenderComponent::Init()
{
}

void RenderComponent::Update()
{
}

void RenderComponent::SetTexture(const std::string& filename)
{
	m_pTexture = ResourceManager::GetInstance().LoadTexture(filename);
}

void RenderComponent::Render() const
{
	const auto collider = m_pParent.lock()->GetComponent<ColliderComponent>();

	if (m_RenderTexture)
	{
		if (collider == nullptr) Renderer::GetInstance().RenderTexture(*m_pTexture, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().x, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().y);
		else Renderer::GetInstance().RenderTexture(*m_pTexture, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().x, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().y, collider->GetSize().x, collider->GetSize().y);
	}
	else if (collider != nullptr) Renderer::GetInstance().RenderFilledRectangle(m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().x, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().y,collider->GetSize().x, collider->GetSize().y, m_RectColor);

}
