#pragma once
#include "BaseComponent.h"
#include "Texture2D.h"
#include <SDL.h>

class RenderComponent :
	public BaseComponent
{
public:
	RenderComponent(std::shared_ptr<GameObject> parent, bool renderTexture, SDL_Color color);
	~RenderComponent();

	void Init() override;
	void Update() override;

	void SetTexture(const std::string& filename);
	void Render() const override;

private:
	std::shared_ptr<Texture2D> m_pTexture{};
	bool m_RenderTexture;
	SDL_Color m_RectColor;
};


