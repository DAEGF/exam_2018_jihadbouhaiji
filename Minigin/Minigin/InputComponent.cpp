#include "MiniginPCH.h"
#include "InputComponent.h"
#include "InputManager.h"
#include <SDL.h>

InputComponent::InputComponent(std::shared_ptr<GameObject> parent) :
BaseComponent(parent),
m_Inputs{},
m_CurrentState{}
{
}

void InputComponent::Init()
{
}

void InputComponent::Update()
{
	
}

void InputComponent::AddInput(Input inputToAdd, std::shared_ptr<Command> commandToAdd)
{
	m_Inputs.push_back(std::make_pair(inputToAdd, commandToAdd));
}


void InputComponent::HandleInput()
{

	auto& input = InputManager::GetInstance();
	/*bool inputDetected = false;*/

	for(unsigned int i{}; i < m_Inputs.size(); i++)
	{
		if (input.IsPressed(m_Inputs[i].first))
		{
			m_Inputs.at(i).second->Execute(m_pParent.lock());
			/*inputDetected = true;*/
		}
	}
	
}




