#pragma once
#include "Singleton.h"
#include <SDL.h>

struct SDL_Window;
struct SDL_Renderer;

class Texture2D;
class Renderer final : public Singleton<Renderer>
{
	SDL_Renderer* m_Renderer = nullptr;

public:
	void Init(SDL_Window* window);
	void Render();
	void Destroy();

	void RenderTexture(const Texture2D& texture, float x, float y) const;
	void RenderTexture(const Texture2D& texture, float x, float y, float width, float height) const;

	void RenderRectangle(float x, float y, float width, float height) const;
	void RenderRectangle(const SDL_Rect& rectToDraw) const;
	void RenderFilledRectangle(float x, float y, float width, float height,const SDL_Color& color) const;

	SDL_Renderer* GetSDLRenderer() const { return m_Renderer; }
};

