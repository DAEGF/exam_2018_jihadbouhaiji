#include "MiniginPCH.h"
#include "TextComponent.h"
#include "Renderer.h"
#include "GameObject.h"

TextComponent::TextComponent(const std::string& text, std::shared_ptr<Font> font, std::shared_ptr<GameObject> parent):
BaseComponent(parent),
m_Text{text},
m_HasChanged{true},
m_Font{font},
m_pTexture{nullptr},
m_Color{255,255,255}
{
}

void TextComponent::Init()
{ 

}

void TextComponent::Update()
{
	if (m_HasChanged)
	{
		const SDL_Color color = m_Color; 
		const auto surf = TTF_RenderText_Blended(m_Font->GetFont(), m_Text.c_str(), color);
		if (surf == nullptr) {
			std::stringstream ss; ss << "Render text failed: " << SDL_GetError();
			throw std::runtime_error(ss.str().c_str());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr) {
			std::stringstream ss; ss << "Create text texture from surface failed: " << SDL_GetError();
			throw std::runtime_error(ss.str().c_str());
		}
		SDL_FreeSurface(surf);
		m_pTexture = std::make_shared<Texture2D>(texture);
		m_HasChanged = false;
	}
}


void TextComponent::Render() const
{
	if (m_pTexture != nullptr)
	{
		Renderer::GetInstance().RenderTexture(*m_pTexture, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().x, m_pParent.lock()->GetComponent<TransformComponent>()->GetPosition().y);
	}
}

void TextComponent::SetText(std::string text)
{
	m_Text = text;
	m_HasChanged = true;
}

void TextComponent::SetFont(std::shared_ptr<Font> font)
{
	m_Font = font;
	m_HasChanged = true;
}

void TextComponent::SetColor(UINT8 r, UINT8 g, UINT8 b)
{
	m_Color = SDL_Color{ r, g, b };
	m_HasChanged = true;
}


TextComponent::~TextComponent()
{
}
