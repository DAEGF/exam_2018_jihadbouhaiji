#include "MiniginPCH.h"
#include "Ghost.h"
#include "RenderComponent.h"
#include "InputComponent.h"
#include "AIInputComponent.h"
#include "Time.h"


Ghost::Ghost(std::shared_ptr<Scene> scene) :
GameObject(scene, "Ghost"),
	m_IsScared{},
	m_ScareTimer{},
	m_ScareTimerMax{ 7.0f }
{
}


Ghost::~Ghost()
= default;


void Ghost::Init(std::shared_ptr<GameObject> self, const glm::vec2& size, const glm::vec2& pos, const bool isAi, Color color)
{
	m_isAi = isAi;

	AddComponent(std::make_shared<RenderComponent>(self,true,SDL_Color{}));
	if(isAi) AddComponent(std::make_shared<AIInputComponent>(self));
	else AddComponent(std::make_shared<InputComponent>(self));
	AddComponent(std::make_shared<PhysicsComponent>(self, size, 20.0f, true));

	GameObject::Init();

	GetComponent<TransformComponent>()->SetPosition(pos.x, pos.y, 0.f);
	switch (color)
	{
	case Color::Red:
		GetComponent<RenderComponent>()->SetTexture("RedGhost.png");
		break;
	case Color::Blue:
		GetComponent<RenderComponent>()->SetTexture("BlueGhost.png");
		break;
	case Color::Orange:
		GetComponent<RenderComponent>()->SetTexture("OrangeGhost.png");
		break;
	case Color::Pink:
		GetComponent<RenderComponent>()->SetTexture("PinkGhost.png");
		break;
		
	}
	
	if(!m_isAi)
	{
		GetComponent<InputComponent>()->AddInput(Input::ButtonY, std::make_unique<UpCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonX, std::make_unique<LeftCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonB, std::make_unique<RightCommand>());
		GetComponent<InputComponent>()->AddInput(Input::ButtonA, std::make_unique<DownCommand>());
	}
}

void Ghost::GetTarget(std::string name)
{
	if(m_isAi) GetComponent<AIInputComponent>()->GetTargetByName(name);
}


	

void Ghost::LateUpdate()
{
	if(m_IsScared && m_isAi)
	{
		auto AiComp = GetComponent<AIInputComponent>();
		switch (AiComp->GetCurrentCommand()) 
		{ 
		case AIInputComponent::Commands::Up:
			AiComp->OverrideCommand(AIInputComponent::Commands::Down);
			break;
		case AIInputComponent::Commands::Down:
			AiComp->OverrideCommand(AIInputComponent::Commands::Up);
			break;
		case AIInputComponent::Commands::Left:
			AiComp->OverrideCommand(AIInputComponent::Commands::Right);
			break;
		case AIInputComponent::Commands::Right: 
			AiComp->OverrideCommand(AIInputComponent::Commands::Left);
			break;

		default:
			break;
		}

		m_ScareTimer += Time::m_FixedDeltaTime;

		if(m_ScareTimer > m_ScareTimerMax)
		{
			m_ScareTimer = 0.0f;
			m_IsScared = false;
		}
		
	}
	GameObject::LateUpdate();
}

void Ghost::SetScared(const bool toSet)
{
	m_IsScared = toSet;
}

bool Ghost::IsScared() const
{
	return m_IsScared;
}


