#pragma once
#include "Singleton.h"

class Scene;
class SceneManager final : public Singleton<SceneManager>
{
public:
	std::shared_ptr<Scene> CreateScene(const std::string& name);

	void Update();
	void LateUpdate();
	void Render();
	void ProcessInput();
	void SetActive(std::shared_ptr<Scene> activeScene);

private:
	std::vector<std::shared_ptr<Scene>> m_Scenes;
	std::weak_ptr<Scene> m_ActiveScene;
};


