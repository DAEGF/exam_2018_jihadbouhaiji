#pragma once
#include <XInput.h>
#include "Singleton.h"

enum class Input
{
	ButtonA,
	ButtonB,
	ButtonX,
	ButtonY,
	
	KeyUp,
	KeyDown,
	KeyLeft,
	KeyRight

};



class InputManager final : public Singleton<InputManager>
{
public:
	bool ProcessInput();
	bool IsPressed(Input toCheck) const;

private:
	XINPUT_STATE m_CurrentState{};
};



 

	